<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBoxesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('boxes', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('numbox')->unique();
			$table->string('owner_name', 100);
			$table->string('owner_lastname', 100);
			$table->date('owner_birthdate');
			$table->text('owner_address');
			$table->string('email', 100)->unique();
			$table->string('twitter', 50)->nullable();
			$table->string('facebook', 100)->nullable();
			$table->string('owner_photo')->default('default_image.jpg');
			$table->string('profession', 100)->nullable();
			$table->string('owner_nid', 30);
			$table->integer('company_id')->unsigned();
			$table->foreign('company_id')
					->references('id')
					->on('companies')
					->onDelete('cascade');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('boxes');
	}

}
