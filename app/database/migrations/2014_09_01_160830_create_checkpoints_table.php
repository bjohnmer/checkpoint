<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCheckpointsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('checkpoints', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name', 50);
			$table->enum('type', array('Received','Shipping'))->default('Received');
			$table->text('description');
			$table->string('icon', 50)->default('checkpoint_default.jpg');
			$table->text('place');
			$table->integer('company_id')->unsigned();
			$table->foreign('company_id')
					->references('id')
					->on('companies')
					->onDelete('cascade');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('checkpoints');
	}

}
