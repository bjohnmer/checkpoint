<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateParcelsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('parcels', function(Blueprint $table)
		{
			$table->increments('id');
			$table->float('volumen');
			$table->float('weight');
			$table->text('observations')->nullable();
			$table->integer('prealert_id')->unsigned();
			$table->foreign('prealert_id')
					->references('id')
					->on('prealerts')
					->onDelete('cascade');
			$table->integer('clasification_id')->unsigned();
			$table->foreign('clasification_id')
					->references('id')
					->on('clasifications')
					->onDelete('cascade');
			$table->integer('company_id')->unsigned();
			$table->foreign('company_id')
					->references('id')
					->on('companies')
					->onDelete('cascade');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('parcels');
	}

}
