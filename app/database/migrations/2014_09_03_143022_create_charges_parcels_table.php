
<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateChargesParcelsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('charges_parcels', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('charge_id')->unsigned();
			$table->foreign('charge_id')
					->references('id')
					->on('charges')
					->onDelete('cascade');
			$table->integer('parcel_id')->unsigned();
			$table->foreign('parcel_id')
					->references('id')
					->on('parcels')
					->onDelete('cascade');
			$table->text('observation');
			$table->integer('company_id')->unsigned();
			$table->foreign('company_id')
					->references('id')
					->on('companies')
					->onDelete('cascade');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('charges_parcels');
	}

}
