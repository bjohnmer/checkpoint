<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTestimonialsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('testimonials', function(Blueprint $table)
		{
			$table->increments('id');
			$table->text('text');
			$table->integer('qualif');
			$table->integer('box_id')->unsigned();
			$table->foreign('box_id')
					->references('id')
					->on('boxes')
					->onDelete('cascade');
			$table->integer('company_id')->unsigned();
			$table->foreign('company_id')
					->references('id')
					->on('companies')
					->onDelete('cascade');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('testimonials');
	}

}
