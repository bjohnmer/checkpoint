<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCheckpointsParcelsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('checkpoints_parcels', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('privilege_id')->unsigned();
			$table->foreign('privilege_id')
					->references('id')
					->on('privileges')
					->onDelete('cascade');
			$table->integer('chekpoint_id')->unsigned();
			$table->foreign('chekpoint_id')
					->references('id')
					->on('checkpoints')
					->onDelete('cascade');
			$table->integer('company_id')->unsigned();
			$table->foreign('company_id')
					->references('id')
					->on('companies')
					->onDelete('cascade');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('checkpoints_parcels');
	}

}
