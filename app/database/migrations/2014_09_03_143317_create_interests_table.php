<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateInterestsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */

	public function up()
	{
		Schema::create('interests', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id')->unsigned();
			$table->foreign('user_id')
					->references('id')
					->on('users')
					->onDelete('cascade');
			$table->integer('company_id')->unsigned();
			$table->foreign('company_id')
					->references('id')
					->on('companies')
					->onDelete('cascade');
			$table->boolean('airplanes')->default(false);
			$table->boolean('aerography')->default(false);
			$table->boolean('acting')->default(false);
			$table->boolean('astronomy')->default(false);
			$table->boolean('animals')->default(false);
			$table->boolean('astrology')->default(false);
			$table->boolean('sports')->default(false);
			$table->boolean('beach')->default(false);
			$table->boolean('beatbox')->default(false);
			$table->boolean('bikes')->default(false);
			$table->boolean('blacksmith')->default(false);
			$table->boolean('disguises')->default(false);
			$table->boolean('bonsai')->default(false);
			$table->boolean('bowling')->default(false);
			$table->boolean('arts')->default(false);
			$table->boolean('camping')->default(false);
			$table->boolean('karts')->default(false);
			$table->boolean('chess')->default(false);
			$table->boolean('religion')->default(false);
			$table->boolean('antiques')->default(false);
			$table->boolean('music')->default(false);
			$table->boolean('computers')->default(false);
			$table->boolean('diy')->default(false);
			$table->boolean('cakes')->default(false);
			$table->boolean('cooking')->default(false);
			$table->boolean('recycling')->default(false);
			$table->boolean('photography')->default(false);
			$table->boolean('dolls')->default(false);
			$table->boolean('drawing')->default(false);
			$table->boolean('electronic')->default(false);
			$table->boolean('gardening')->default(false);
			$table->boolean('movies')->default(false);
			$table->boolean('horses')->default(false);
			$table->boolean('hunting')->default(false);
			$table->boolean('internet')->default(false);
			$table->boolean('science')->default(false);
			$table->boolean('puzzles')->default(false);
			$table->boolean('kites')->default(false);
			$table->boolean('languages')->default(false);
			$table->boolean('lego')->default(false);
			$table->boolean('airmodelling')->default(false);
			$table->boolean('painting')->default(false);
			$table->boolean('origami')->default(false);
			$table->boolean('skydiving')->default(false);
			$table->boolean('reading')->default(false);
			$table->boolean('robotics')->default(false);
			$table->boolean('extreme_sports')->default(false);
			$table->boolean('videogames')->default(false);
			$table->boolean('business')->default(false);
			$table->boolean('woodworking')->default(false);
			$table->boolean('yoga')->default(false);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('interests');
	}

}
