<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePrealertsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('prealerts', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('tracking_number', 50);
			$table->string('courier', 50);
			$table->string('product_name', 150);
			$table->string('invoice_file', 100)->nullable();
			$table->string('shop', 50);
			$table->string('url_shop');
			$table->text('observations')->nullable();
			$table->integer('company_id')->unsigned();
			$table->foreign('company_id')
					->references('id')
					->on('companies')
					->onDelete('cascade');
			$table->integer('box_id')->unsigned();
			$table->foreign('box_id')
					->references('id')
					->on('boxes')
					->onDelete('cascade');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('prealerts');
	}

}
