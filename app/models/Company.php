<?php

class Company extends \Eloquent {


 protected $fillable =['name','logo','contact_page','goals_page','address','telf','email'];
 protected $guarded = ['id'];

  // Add your validation rules here
  public static $rules = [
    'name' => 'required|min:5|max:100',
    'logo'  => 'required|mimes:jpg,jpeg,png',
    'contact_page' => 'required|min:5|max:100',
    'goals_page' => 'required|min:5|max:100',
    'address' => 'required|min:5|max:100',
    'email'  => 'required|email|min:6|max:40',
    'telf'  => 'required|numeric',
    
  ];
   public static $rulesUpdate = [
    'name' => 'required|min:5|max:100',
    'logo'  => 'mimes:jpg,jpeg,png',
    'contact_page' => 'required|min:5|max:100',
    'goals_page' => 'required|min:5|max:100',
    'address' => 'required|min:5|max:100',
    'email'  => 'required|email|min:6|max:40',
    'telf'  => 'required|numeric',

   ];



  public static function validator($data)
   {
    return Validator::make($data, static::$rules);
   }

   public static function validatorUpdate($data)
   {
    return Validator::make($data, static::$rulesUpdate);
   }



}