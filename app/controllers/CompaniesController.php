<?php

class CompaniesController extends \BaseController {

	/**
	 * Display a listing of companies
	 *
	 * @return Response
	 */
	public function index()
	{
		$companies = Company::all();

		return View::make('backend.companies.index', compact('companies'));
	}

	/**
	 * Show the form for creating a new company
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('backend.companies.create');
	}

	/**
	 * Store a newly created company in storage.
	 *
	 * @return Response
	 */
	
	/*
	public function store()
	{
		$validator = Validator::make($data = Input::all(), Company::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		Company::create($data);

		return Redirect::route('backend.companies.index');
	}
	*/

	public function store()
	{
		
		$validator =Company::validator(Input::all());
		

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}
		
		$datos = Input::all();

		if (Input::file('logo'))
		{
			$file = Input::file('logo');		
			$destinationPath = 'uploads/images/';
			$filename = Str::random(20) .'.'. $file->getClientOriginalExtension();
			$mimeType = $file->getMimeType();
			$extension = $file->getClientOriginalExtension();
			$upload_success = $file->move($destinationPath,$filename);
			$datos['logo'] = $filename;
		}
	
		
		Company::create($datos);
		Session::flash('message','Guardado Correctamente');
		Session::flash('class','success');
		

		//return Redirect::route('companies');
		return Redirect::route('companies.index');
	}

	/**
	 * Display the specified company.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$company = Company::findOrFail($id);

		return View::make('backend.companies.show', compact('company'));
	}

	/**
	 * Show the form for editing the specified company.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$company = Company::find($id);

		return View::make('backend.companies.edit', compact('company'));
	}

	/**
	 * Update the specified company in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */

	public function update($id)
	{

		$company = Company::findOrFail($id);
	
		$validator =Company::validatorUpdate(Input::all());

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$datos = Input::all();

		if (Input::file('logo')) {
			$file = Input::file('logo');		
			$destinationPath = 'uploads/images/';
			$filename = Str::random(20) .'.'. $file->getClientOriginalExtension();
			$mimeType = $file->getMimeType();
			$extension = $file->getClientOriginalExtension();
			$upload_success = $file->move($destinationPath,$filename);
			
			File::delete($destinationPath.$company->logo);
			
			$datos['logo'] = $filename;
		}
		else
		{
			unset($datos['logo']);
		}
		
		$company->update($datos);

		Session::flash('message','¡Actualizado Correctamente!');
		Session::flash('class','success');
		return Redirect::route('companies.index');
		
	}

	/**
	 * Remove the specified company from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Company::destroy($id);

		return Redirect::route('companies.index');
	}

}
