@extends("backend.layout")

@section("content")
  
    <ol class="breadcrumb">
      <li><a href="{{ URL::to('/dashboard') }}">Dashboard</a></li>
      <li><a href="{{ URL::to('/companies') }}">Companies</a></li>
      <li class="active">New Company</li>
    </ol>

  <h1>New Company</h1>
  <div class="row">
    <div class="col-sm-12">&nbsp;</div>
  </div>
  @if(Session::has('message'))  <!--muestra mesaje de suceso que viene del homecontrol-->
        <div class="alert alert-{{ Session::get('class') }} fade in">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
            <p>  {{ Session::get('message') }} </p>
        </div>
    @endif
  @if($errors->has())               
       <div class="alert alert-danger fade in">
       <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
       @foreach($errors->all() as $error)
          <p>{{ $error  }}</p>
       @endforeach
     </div>
  @endif
    {{ Form::open(['url'=>'/companies','method'=>'post', 'file'=>true, 'enctype' => 'multipart/form-data']) }}
    
    <div class="form-group">  
      <input type="file" name="logo" id="logo" value='{{ Input::old('logo') }}'>
    </div>

    <div class="form-group">
      <label for="name">Company Name</label>
      <input type="text" name="name" placeholder="Compnay Name" value="{{ Input::old('name')}}" class="form-control">
    </div >
                  
    <div class="form-group">
     <label for="contact_page">Contact Page</label>
     <textarea id="contact_page" name="contact_page" placeholder="Contact Page" class="form-control">{{ Input::old('contact_page')}}</textarea>
    </div >

    <div class="form-group">
     <label for="goals_page">Goals Page</label>
     <textarea   id="goals_page"  name="goals_page" placeholder="Goals Page"  class="form-control">{{ Input::old('goals_page')}}</textarea>
    </div >

     <div class="form-group">
     <label for="address">Address</label>
     <input type="text" id="address" name="address" placeholder="Address" value="{{ Input::old('address')}}" class="form-control">
    </div >

     <div class="form-group">
     <label for="telf">Telephone</label>
     <input type="text" name="telf" placeholder="Telephone" value="{{ Input::old('telf')}}" class="form-control">
    </div >

     <div class="form-group">
     <label for="email">Email</label>
     <input type="email" name="email" placeholder="Email" value="{{ Input::old('email')}}" class="form-control">
    </div >
   
      <button type="submit" class="btn btn-success">
      <span class="glyphicon glyphicon-save"> Save</span>
      </button>
        
      
      {{ Form::close() }}

@stop