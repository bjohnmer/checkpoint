@extends("backend.layout")

@section("content")
  <ol class="breadcrumb">
    <li><a href="{{ URL::to('/dashboard') }}">Dashboard</a></li>
    <li class="active">Companies</li>
  </ol>

  <h1>Companies</h1>
   @if(Session::has('message'))  <!--muestra mesaje de suceso que viene del homecontrol-->
        <div class="alert alert-{{ Session::get('class') }} fade in">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
            <p>  {{ Session::get('message') }} </p>
        </div>
    @endif
  <div class="row">
    <div class="col-sm-12">
      <a href="{{ URL::to('/companies/create') }}" class="btn btn-success" alt="New Company" title="New Company">
        <span class="glyphicon glyphicon-plus"></span>
      </a>
    </div>
  </div>
  <div class="table-responsive">
      <table class="table">
        <thead>
            <tr>
              <th width="5%">#</th>
              <th width="15%">&nbsp; logo</th>
              <th width="15%">Company Name</th>
              <th width="15%">Address</th>
              <th width="10%">Telephones</th>
              <th width="20%">Email</th>
              <th width="20%">Actions</th>
            </tr>
        </thead>
              <tbody>
                    @foreach ($companies as $c)
                    <tr>
                      <td> {{ $c->id }} </td>
                      <td><img src="{{URL::to('/')}}/uploads/images/{{$c->logo}}" alt="{{$c->name}}" title="{{$c->name}}" class="img-responsive img-list"></td>
                      <td> {{ $c->name}} </td>
                      <td> {{ $c->address}} </td>
                      <td> {{ $c->telf}} </td>
                      <td> {{ $c->email}} </td>
                      <td>
                        {{ Form::open(['url'=>'/companies/'.$c->id,'method'=>'DELETE']) }}
                        <a href="{{ URL::to('/companies/'.$c->id.'/edit') }}" class="btn btn-warning">
                          <span class="glyphicon glyphicon-edit"></span>
                        </a>
                        <button class="btn btn-danger">
                          <span class="glyphicon glyphicon-remove"></span>
                        </button>
                        {{ Form::close() }}
                      </td>
                    </tr>
                    @endforeach
              </tbody>
      </table>
    </div>


@stop